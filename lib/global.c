#include "global.h"
#include <stdio.h>

static FILE *fd;

inline void logs(char* s) {
    if (!fd) {
        fd = fopen("log.txt", "a");
    }
    fprintf(fd,"%s",s);
    fflush(fd);
}

inline void logi(int i) {
    if (!fd) {
        fd = fopen("log.txt", "a");
    }
    fprintf(fd,"%d",i);
    fflush(fd);
}

inline void logsn(char* s) {
    if (!fd) {
        fd = fopen("log.txt", "a");
    }
    fprintf(fd,"%s\n",s);
    fflush(fd);
}

inline void logh(char h) {
    if (!fd) {
        fd = fopen("log.txt", "a");
    }
    fprintf(fd,"0x%X ",h);
    fflush(fd);
}

inline void logend() {
    if (!fd) {
        fd = fopen("log.txt","a");
    }
    fprintf(fd,"\n");
    fflush(fd);
    fclose(fd);
    fd = 0;
}

