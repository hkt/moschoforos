#ifndef __GLOBAL_H__
#define __GLOBAL_H__

inline void logs(char*);
inline void logi(int);
inline void logsn(char*);
inline void logh(char);
inline void logend();

#endif /* __GLOBAL__H__ */
