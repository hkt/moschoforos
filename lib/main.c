#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <netdb.h> 
#include <stdio.h>
#include <stdlib.h>

#define COMM_PORT 2012

int main(int argc, char * argv[]) {
	if (argc < 2) {
		fprintf(stderr,"usage %s hostname\n", argv[0]);
		exit(0);
	}	
	int sockfd, portno, n;
	struct sockaddr_in serv_addr;
	struct hostent *server;
	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (sockfd < 0) 
	{
		logs("ERROR opening socket");
		logend();
		return;
	}
	server = gethostbyname(argv[1]);
	if (server == NULL) 
	{
		logs("ERROR, no such host: ");
		logs(argv[1]);
		logend();
		return;
	}
	bzero((char *) &serv_addr, sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	bcopy((char *)server->h_addr, 
			(char *)&serv_addr.sin_addr.s_addr,
			server->h_length);
	serv_addr.sin_port = htons(COMM_PORT);
	if (connect(sockfd,(struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0) 
	{
		logs("ERROR on connecting");
		logend();
		return;
	}
	procesor((void *)&sockfd);
	close(sockfd);
	logend();
	exit(EXIT_SUCCESS);
}
