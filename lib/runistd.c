#include "runistd.h"
#include "messages.h"
#include "global.h"
#define COMM_PORT 2012

char* enc_int(char *s,int32_t num) {
	union {
		int32_t i;
		char c[4];
	} bint;
	bint.i = htonl(num);
	memcpy(s,bint.c,4*sizeof(char));
	return s+4;
}

int32_t dec_int(char* s) {
	union {
		int32_t i;
		char c[4];
	} bint;
	memcpy(bint.c,s,4*sizeof(char));
	bint.i = ntohl(bint.i);
	return bint.i;
}

int conn(const char *hostname, const char *username){
	char * buffer = (char*)malloc(MESSAGE_SIZE*sizeof(unsigned char));
	int sockfd, portno, n;
	struct sockaddr_in serv_addr;
	struct hostent *server;
	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (sockfd < 0) 
	{
		logs("ERROR opening socket");
		logend();
		return -1;
	}
	server = gethostbyname(hostname);
	if (server == NULL) 
	{
		logs("ERROR, no such host: ");
		logs((char*)hostname);
		logend();
		return -1;
	}
	bzero((char *) &serv_addr, sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	bcopy((char *)server->h_addr, 
			(char *)&serv_addr.sin_addr.s_addr,
			server->h_length);
	serv_addr.sin_port = htons(COMM_PORT);
	if (connect(sockfd,(struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0) 
	{
		logs("ERROR on connecting");
		logend();
		return -1;
	}

	//Verify
	bzero(buffer,MESSAGE_SIZE);
	buffer[0] = REQ_IDENTIFY;
	buffer[1] = MAGIC_NUMBER;
	strcpy(buffer+H_SIZE,username);

	int i;
	n = write(sockfd,buffer,MESSAGE_SIZE);
	if ( n < 0 )
	{
		logs("ERROR sending identity: ");
		logs(buffer);
		logend();
		return -1;
	}

	bzero(buffer,MESSAGE_SIZE);
	n = read(sockfd, buffer, MESSAGE_SIZE);
	if ( n < 0 )
	{
		logs("ERROR reading from socket: |");
		logend();
		return -1;
	}

	logi(n);logsn(" bytes received.");
	if ( buffer[0] != RES_IDENTIFY )
	{
		logs("Unknown message: |");
		logs(buffer);
		logend();
		return -1;
	}

	if ( buffer[1] == OPERATION_NOPE || buffer[1] != OPERATION_OK)
	{
		logs("Authorization failed");
		logi(buffer[1]);
		logend();
		logi(strlen(buffer));
		logend();
		return -1;
	}

	logs("Authorization succesful");
	logend();
	return sockfd;
	free(buffer);
}

int disco(int fd_con){
	int n;
	char * buffer = malloc(MESSAGE_SIZE*sizeof(unsigned char));
	buffer[0] = REQ_BYE;
	buffer[1] = MAGIC_NUMBER;

	n = write(fd_con,buffer,MESSAGE_SIZE);
	if(n<0){
		return -1;
	}
	free(buffer);
	return close(fd_con);
}

int ropen(int fd_con, const char *pathname, int flags){
	int n;
	unsigned char buffer[MESSAGE_SIZE] = {0};

	//set message to buffer
	buffer[0]=REQ_OPEN;
	buffer[1]=MAGIC_NUMBER;

	//set flags
	char * p = enc_int(buffer+H_SIZE,flags); 

	//copy pathname
	strcpy(p,pathname);

	//send message
	n = write(fd_con,buffer,MESSAGE_SIZE);
	if(n<0){
		return -1;
	}

	bzero(buffer,MESSAGE_SIZE);
	n = read(fd_con,buffer, MESSAGE_SIZE);
	if(n<0){
		return -1;	
	}
    // Wrong response
	if(buffer[0]!=RES_OPEN){
		return -1;
	}
    // Don't have rights
    if(buffer[0]==RES_OPEN && buffer[1]==OPERATION_NOPE) {
        return -2;
    }

    if(buffer[0]==RES_OPEN && buffer[1]==OPERATION_OK) 
        return dec_int(buffer+H_SIZE);
    else
        return -2;
}

int rclose(int fd_con, int fd_id){
	int n;
	unsigned char buffer[MESSAGE_SIZE] = {0};

	//set message to buffer
	buffer[0]=REQ_CLOSE;
	buffer[1]=MAGIC_NUMBER;

	//set file descriptor
	char * p = enc_int(buffer+H_SIZE,fd_id);

	//send message
	n = write(fd_con,buffer,MESSAGE_SIZE);
	if(n<0){
		return -1;
	}
	//fill buffer with zeros & receive close response
	bzero(buffer,MESSAGE_SIZE);
	n = read(fd_con,buffer, MESSAGE_SIZE);
	if(n<0){
		return -1;	
	}
	if(buffer[0]!=RES_CLOSE){
		return -1;
	}
	return 0;
}

ssize_t rread(int fd_con, int fd_id, void *buf, size_t req_count){
	int n;
	unsigned char buffer[MESSAGE_SIZE] = {0};
	int chunksize = 0;
	int sumsize = 0;
	void* pos = buf;

	//return if asked for < 4GB
	if( req_count > UINT_MAX )
		return -1;

	while (sumsize < req_count) {
		buffer[0]=REQ_READ; //header read
		buffer[1]=MAGIC_NUMBER; //header second byte
		enc_int(buffer+H_SIZE,fd_id);
		enc_int(buffer+H_SIZE+I_SIZE, req_count-sumsize);  
		/* (req_count-sumsize) can be greater than 
		 * MESSAGE_SIZE but we don't care
		 * server can send only MESSAGE_SIZE-H_SIZE-I_SIZE chunks
		 */
		n = write(fd_con,buffer,MESSAGE_SIZE);
		if(n<0){
			logsn("error sending READ request");
			return n;
		}
		//bzero(buffer, MESSAGE_SIZE);
		n = read(fd_con,buffer,MESSAGE_SIZE);
		if(n<0){
			return n;	
		}
		if(buffer[0]!=RES_READ){
			logsn("Received different message than RES_READ");
			return -1;
		}

		chunksize = dec_int(buffer+H_SIZE);
		if (chunksize == 0)
			return sumsize;
		if (chunksize == -1){
			logsn("Server could not perform read");
			return -1;
		}

		memcpy(pos,buffer+H_SIZE+I_SIZE,chunksize*sizeof(unsigned char));
		pos += chunksize;
		sumsize += chunksize;
	}
	return sumsize;
}

ssize_t rwrite(int fd_con, int fd_id, const void *buf, size_t req_count){
	int n,i;
	unsigned char buffer[MESSAGE_SIZE] = {0};
	int chunkcount; //how many chunks we have to send
	int wrbytessize = 0; //bytes written in one chunk
	int sumsize = 0;
	void * pos = (void*)buf;

	//return if asked for < 4GB
	if( req_count > UINT_MAX )
		return -1;

	//count number of chunks to send
	chunkcount = (req_count /(MESSAGE_SIZE-H_SIZE-I_SIZE-I_SIZE))+1;
	logs("Chunks count: ");
	logi(chunkcount);	
	logend();

	//send n chunks
	for(i=0;i<chunkcount;i++){
		buffer[0]=REQ_WRITE; //header write
		buffer[1]=MAGIC_NUMBER; //header second byte
		enc_int(buffer+H_SIZE,fd_id);

		if(i<chunkcount-1)
			enc_int(buffer+H_SIZE+I_SIZE, MESSAGE_SIZE-H_SIZE-I_SIZE-I_SIZE);
		else
			enc_int(buffer+H_SIZE+I_SIZE, req_count % (MESSAGE_SIZE-H_SIZE-I_SIZE-I_SIZE));

		memcpy(buffer+H_SIZE+I_SIZE+I_SIZE, pos, (MESSAGE_SIZE-H_SIZE-I_SIZE-I_SIZE)*sizeof(unsigned char));

		n = write(fd_con,buffer,MESSAGE_SIZE);
		if(n<0){
			logsn("error sending WRITE request");
			return n;
		}

		n = read(fd_con,buffer,MESSAGE_SIZE);
		if(n<0){
			return n;	
		}

		if(buffer[0]!=RES_WRITE){
			logsn("Received different message than RES_WRITE");
			return -1;
		}

		wrbytessize=dec_int(buffer+H_SIZE);
		//if chunksize too low & we are not in last chunk
		if((wrbytessize< MESSAGE_SIZE-H_SIZE-I_SIZE-I_SIZE) && (i<chunkcount-1)){
			logs("Error: Got too little chunk size: ");logi(wrbytessize);
			logs("on chunk number: ");logi(i);logend();
			return -1;
		}
		sumsize += wrbytessize;
		pos+= wrbytessize;
	}
	return sumsize;
}

off_t rlseek(int fd_con, int fd_id, off_t offset, int whence){
	int n;
	unsigned char buffer[MESSAGE_SIZE] = {0};

	//set message to buffer
	buffer[0]=REQ_LSEEK;
	buffer[1]=MAGIC_NUMBER;

	//set file descriptor
	enc_int(buffer+H_SIZE,fd_id);

	//set offset
	enc_int(buffer+H_SIZE+I_SIZE,offset);

	//set flag
	enc_int(buffer+H_SIZE+I_SIZE+I_SIZE,whence);

	//send message
	n = write(fd_con,buffer,MESSAGE_SIZE);
	if(n<0){
		logsn("Error on sending REQ_LSEEK");
		return -1;
	}

	//fill buffer with zeros & receive close response
	bzero(buffer,MESSAGE_SIZE);
	n = read(fd_con,buffer, MESSAGE_SIZE);
	if(n<0){
		logsn("Error on reading RES_LSEEK");
		return -1;	
	}
	if(buffer[0]!=RES_LSEEK){
		logsn("Received something but not RES_LSEEK");
		return -1;
	}

	return dec_int(buffer+H_SIZE);
}

int rcd(int fd_con, const char *directory){
	int n;
	unsigned char buffer[MESSAGE_SIZE] = {0};

	//set message to buffer
	buffer[0]=REQ_CD;
	buffer[1]=MAGIC_NUMBER;

	//set directory to buffer
	strcpy(buffer+H_SIZE,directory);

	//send message
	n = write(fd_con,buffer,MESSAGE_SIZE);
	if(n<0){
		logsn("Error on sending REQ_CD");
		return -1;
	}

	//fill buffer with zeros & receive cd response
	bzero(buffer,MESSAGE_SIZE);
	n = read(fd_con,buffer, MESSAGE_SIZE);
	if(n<0){
		logsn("Error on reading RES_CD");
		return -1;	
	}
	if(buffer[0]!=RES_CD){
		logsn("Received something but not RES_CD");
		return -1;
	}
	if ( buffer[1] == OPERATION_NOPE || buffer[1] != OPERATION_OK)
	{
		logsn("CD failed");
		return -1;
	}

	return 0;	
}

int rls(int fd_con, const char *directory, char* buf){
	int n;
	char * p=buf;
	unsigned char buffer[MESSAGE_SIZE] = {0};

	//set message to buffer
	buffer[0]=REQ_LS;
	buffer[1]=MAGIC_NUMBER;

	//set directory to buffer
	strcpy(buffer+H_SIZE,directory);

	//send message
	n = write(fd_con,buffer,MESSAGE_SIZE);
	if(n<0){
		logsn("Error on sending REQ_LS");
		return -1;
	}

	//fill buffer with zeros & receive ls response
	bzero(buffer,MESSAGE_SIZE);
	n = read(fd_con,buffer, MESSAGE_SIZE);
	if(n<0){
		logsn("Error on reading RES_LS");
		return -1;	
	}
	if(buffer[0]!=RES_LS){
		logs("Received something but not RES_LS");
		return -1;
	}
	if ( buffer[1] == OPERATION_NOPE || buffer[1] != OPERATION_OK)
	{
		logsn("LS failed on directory");
		return -1;
	}
	strcat(buf,buffer+H_SIZE);

	//read ls entry in loop
	while(buffer[0]==RES_LS && buffer[2]!='\0'){
	
		bzero(buffer,MESSAGE_SIZE);
		n=read(fd_con,buffer,MESSAGE_SIZE);
		logs("Przeczytalem bajtow: ");logi(n);logend();
		if(n<0){
			logsn("Error on reading RES_LS");
			continue;
		}
		if(buffer[0]!=RES_LS){
			logs("Received something but not RES_LS while reading entry: ");
			logi(buffer[0]);
			continue;
		}

		if ( buffer[1] == OPERATION_NOPE || buffer[1] != OPERATION_OK)
		{
			logsn("LS failed on entry");
			continue;
		}
		strcat(buf,buffer+H_SIZE);
	}
	return 0;
}

int rpwd(int fd_con, char* buf){
	int n;
	unsigned char buffer[MESSAGE_SIZE] = {0};

	//set message to buffer
	buffer[0]=REQ_PWD;
	buffer[1]=MAGIC_NUMBER;

	//send message
	n = write(fd_con,buffer,MESSAGE_SIZE);
	if(n<0){
		logsn("Error on sending REQ_PWD");
		return -1;
	}

	//fill buffer with zeros & receive pwd response
	bzero(buffer,MESSAGE_SIZE);
	n = read(fd_con,buffer, MESSAGE_SIZE);
	if(n<0){
		logsn("Error on reading RES_PWD");
		return -1;	
	}
	if(buffer[0]!=RES_PWD){
		logsn("Received something but not RES_PWD");
		return -1;
	}
	if ( buffer[1] == OPERATION_NOPE || buffer[1] != OPERATION_OK)
	{
		logsn("PWD failed");
		return -1;
	}
	memcpy(buf,buffer+H_SIZE,(MESSAGE_SIZE-H_SIZE)*sizeof(unsigned char));
	return 0;
}
