#ifndef __RUNISTD__H__
#define __RUNISTD__H__

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h> //for: mode_t, ssize_t, off_t O_FLAGS
#include <string.h> //for: strcpy, bzero
#include <limits.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>

#define MAGIC_NUMBER 128
#define MESSAGE_SIZE 1024


/* FLAGI */

#define O_READ 0x01
#define O_WRITE 0x02
#define O_TRUNCATE 0x04
#define O_CREATE 0x08
#define O_ADD    0x10

/* FUNKCJE */

int conn(const char *, const char *);
int disco(int);
int ropen(int , const char * , int );
int rclose(int , int );
ssize_t rread(int , int , void * , size_t );
ssize_t rwrite(int , int , const void * , size_t );
off_t rlseek(int , int , off_t , int );
int rcd(int , const char *);
int rls(int , const char *, char *);
int rpwd(int , char *); 
#endif
