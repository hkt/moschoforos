#include "thread.h"
#include <stdlib.h>
#include "global.h"
#include "messages.h"
#include <stdio.h>
#include <string.h>
#include <unistd.h> // lseek flags
/** Message type char(0-255)
 * client array a*2+1
 * server array a*2
 **/

#define MESSAGE_SIZE 1024

struct connection_en {
	int con_fd;
	char status; //0 - closed; 1 - active; 2 - doubtful; 3 - closing
};

typedef struct connection_en connection;

void* procesor(void *socket_fd)
{
	int n;
	int main_socket = *((int*) socket_fd);
	unsigned char buffer[MESSAGE_SIZE];
	connection *client = (connection*) malloc(sizeof(connection));
	bzero(client,sizeof(connection));
	client->con_fd = main_socket;
	client->status = 1;

	//Verify
	unsigned char command[MESSAGE_SIZE];
	printf("Identify yourself: ");
	bzero(buffer,MESSAGE_SIZE);
	buffer[0] = REQ_IDENTIFY;
	buffer[1] = ' ';
    char* p,*line=NULL;
    logsn("Przed getlinem");
	getline(&line,(size_t*)&n,stdin);
    for (p=line; *p ; ++p) 
        if (*p=='\r' || *p=='\n' || *p==' ') {
            *p='\0'; break;
        }
    strncpy(buffer+H_SIZE,line,MESSAGE_SIZE-H_SIZE);
    free(line);
	int i;
	n = write(main_socket,buffer,MESSAGE_SIZE);
	if ( n < 0 )
	{
		logs("ERROR sending identity: ");
		logs(buffer);
		logend();
		return;
	}
	bzero(buffer,MESSAGE_SIZE);
	n = read(main_socket, buffer, MESSAGE_SIZE);
	if ( n < 0 )
	{
		logs("ERROR reading from socket: |");
		logend();
		return;
	}
	logi(n);logsn(" bytes received.");
	if ( buffer[0] != RES_IDENTIFY )
	{
		logs("Unknown message: |");
		logs(buffer);
		logend();
		return;
	}
	if ( buffer[1] == IDENTIFY_NOPE || buffer[1] != IDENTIFY_OK)
	{
		logs("Authorization failed");
		logi(buffer[1]);
		logend();
		logi(strlen(buffer));
		logend();
		return;
	}
	logs("Authorization succesful");
	logend();

	//command loop
	while(1){
		printf("Choose command:\n");
		printf("1: open\n");
        printf("11: open for read\n");
		printf("2: close\n");
		printf("3: read\n");
		printf("4: write\n");
		printf("5: lseek\n");
		printf("6: rcat\n");
		printf("7: read&write\n");

		int cmd;
		scanf("%d", &cmd);


		int descriptor,size;	
		char * pathname;
		char * buff = malloc(1024*1024*sizeof(char));
		switch(cmd)
		{
			case 1:
				//open
				printf("Enter path and filename");
				scanf("%s",pathname);
				descriptor = ropen(client->con_fd,pathname,O_WRITE|O_CREATE);				
				if(descriptor>0){
					logs("Got desrciptor: ");
					logi(descriptor);
					logend();
				}else{
                    if (descriptor == -2) logsn("Not enough privileges");
					logsn("Error on open");				
				}	

				break;

            case 11:
                printf("Enter path: ");
                scanf("%s",pathname);
                descriptor = ropen(client->con_fd,pathname,O_READ);
                if (descriptor<0) {
                    if (descriptor == -2) logsn("Not enough privileges");
                    logsn("Wrong descriptor: ");
                }
                else logi(descriptor);
                break;
			case 2:
				//close
				if(rclose(client->con_fd,descriptor)==0)
					logsn("File closed");
				else
					logsn("Error on closing");
				break;

			case 3:
				//read
                size = rread(client->con_fd,descriptor,buff,MESSAGE_SIZE);
				if(size==-1){
					logsn("Error on reading");
					logi(size);
					logend();
				}else
				{
					logsn("Got buffer: ");
					logs(buff);
					logs(" size: ");
					logi(size);
					logend();
				}
				break;

			case 4:
				//write
				buff ="Ala ma kota";	
				size = rwrite(client->con_fd, descriptor, buff,12);
				if(size==-1){
					logs("Error on writing, size: ");
					logi(size);
					logend();
				}else{
					logs("Write successful, bytes written: ");
					logi(size);
					logend();
				}
				if(rclose(client->con_fd,descriptor)==0)
					logsn("File closed");
				else
					logsn("Error on closing");
				break;

			case 5:
				//lseek
				descriptor = ropen(client->con_fd,"plik.txt",O_READ);				
				if(descriptor>=0){
					logs("Got desrciptor: ");
					logi(descriptor);
					logend();
				}else{
					logs("Error on open");				
				}
				n = rlseek(client->con_fd,descriptor,2,SEEK_SET);
				if(n<0){
					logs("Error :");
					logi(n);
					logend();
				}

				size = rread(client->con_fd,descriptor,buff,MESSAGE_SIZE);
				if(size==-1){
					logsn("Error on reading");
					logi(size);
					logend();
				}else
				{
					logsn("Got buffer: ");
					logs(buff);
					logs(" size: ");
					logi(size);
					logend();
				}
				break;

				

				
				break;
			case 6:
				//rcat
				pathname="plik2.txt";
				descriptor = ropen(client->con_fd,pathname,O_READ);				
				if(descriptor>0){
					logs("Got desrciptor: ");
					logi(descriptor);
					logend();
				}else{
					logs("Error on open");				
				}
				size = MESSAGE_SIZE;
				while ( size >0 ){
					bzero(buf,1024*1024*sizeof(char));
					size = rread(client->con_fd,descriptor,buff,MESSAGE_SIZE);
					if(size==-1){
						logsn("Error on reading");
						logi(size);
						logend();
					}else
					{
						logs(buff);
					}
				}
				if(rclose(client->con_fd,descriptor)==0)
					logsn("File closed");
				else
					logsn("Error on closing");

				break;
			case 7:
				//read&write
				pathname="plik.txt";
				descriptor = ropen(client->con_fd,pathname,O_READ);				
				if(descriptor>0){
					logs("Got desrciptor: ");
					logi(descriptor);
					logend();
				}else{
					logs("Error on open");				
				}	
				size = rread(client->con_fd,descriptor,buff,MESSAGE_SIZE*2);
				if(size==-1){
					logsn("Error on reading");
					logi(size);
					logend();
				}else
				{
					logsn("Got buffer: ");
					logs(buff);
					logs(" size: ");
					logi(size);
					logend();
				}
				if(rclose(client->con_fd,descriptor)==0)
					logsn("File closed");
				else
					logsn("Error on closing");
			
				pathname="nieplik.txt";
				descriptor = ropen(client->con_fd,pathname,O_WRITE|O_CREATE);				
				if(descriptor>0){
					logs("Got desrciptor: ");
					logi(descriptor);
					logend();
				}else{
					logs("Error on open");				
				}	
				size = rwrite(client->con_fd, descriptor, buff,MESSAGE_SIZE+500);
				if(size==-1){
					logs("Error on writing, size: ");
					logi(size);
					logend();
				}else{
					logs("Write successful, bytes written: ");
					logi(size);
					logend();
				}

				if(rclose(client->con_fd,descriptor)==0)
					logsn("File closed");
				else
					logsn("Error on closing");
				break;

			default:
				printf("Unknown command.");
				break;
		}	

	}

	/*
	   while(client->status != CLOSED )
	   {
	   bzero(buffer,MESSAGE_SIZE);
	   n = read(client->con_fd, buffer, MESSAGE_SIZE);
	   if ( n<0 )
	   {
	   client->status = CLOSED;
	   break;
	   }
	   logsn("We got message!");
	   logi(n);
	   logend();
	   switch(buffer[0])
	   {
	   case REQ_PING:
	   logs("replying with PONG!\n");
	   buffer[0] = RES_PONG;
	   n=write(client->con_fd, buffer, 1);
	   if ( n < 0 )
	   {
	   client->status = CLOSED;
	   }
	   break;
	   default:
	   logs("Unknown message type: ");
	   logi(buffer[0]);
	   logs(" val: ");
	   logs(buffer+H_SIZE);
	   logend();
	   break;
	   }
	   }*/
	return;
}
