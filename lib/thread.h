#ifndef __THREAD__H_
#define __THREAD__H_
#include <pthread.h>

//status
#define CLOSED 0
#define ACTIVE 1
#define DOUBTFUL 2
#define CLOSING 3

void* receiver();
void* sender();
void* procesor();

#endif
