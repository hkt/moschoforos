#include <stdio.h>
#include <string.h>
#include <errno.h>
#include "../lib/runistd.h"

int conn_dsc;

void cat_it_net(int *fdsc)
{
  char buf[BUFSIZ];
  bzero(buf,BUFSIZ*sizeof(char));
  while((rread(conn_dsc, *fdsc, buf, BUFSIZ*sizeof(char)))>0)
    fputs(buf, stdout);
}

void cat_it(FILE *fp)
{
  char buf[BUFSIZ];

  while(fgets(buf, sizeof(buf), fp))
    fputs(buf, stdout);
}

int main(int argc, char **argv)
{
  if (argc < 2) {
      fprintf(stderr,"No host name\n");
      exit(1);
  }
  if(argc < 3)
    cat_it(stdin);
  else
  {
      conn_dsc=conn(argv[1],"akasaka");
      if (conn_dsc ==-1) {
        fprintf(stderr,"Couldn't connect\n");
        exit(0);
      }
      int filedescriptor;
      int i;
      for(i = 2;argv[i];++i)
        {
          filedescriptor = ropen(conn_dsc,argv[i],O_READ);
          if(filedescriptor == -1)
            fprintf(stderr, "rcaterror: %s: %s\n", argv[i], strerror(errno));
          else
          {
            cat_it_net(&filedescriptor);
            rclose(conn_dsc,filedescriptor);
          }
        }
      disco(conn_dsc);
  }

  return 0;
}
