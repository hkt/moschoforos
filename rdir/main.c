#include <stdio.h>
#include <string.h>
#include <errno.h>
#include "../lib/runistd.h"

int conn_dsc;

//functional test for rls rpwd & rcd
//usage rdir hostname username directory
int main(int argc, char **argv)
{
	if (argc < 2) {
		fprintf(stderr,"No host name specified\n");
		exit(1);
	}
	if(argc <3){
		fprintf(stderr, "No username specified\n");
		exit(1);
	}
	if(argc < 4){
		fprintf(stderr, "No directory specified\n");
		exit(1);  
	} else {
		conn_dsc=conn(argv[1],argv[2]);
		if (conn_dsc ==-1) {
			fprintf(stderr,"Couldn't connect\n");
			exit(0);
		}
		char * buffer = (char *)malloc(MESSAGE_SIZE*MESSAGE_SIZE*sizeof(unsigned char));
		rpwd(conn_dsc,buffer); 	  
//		printf("%s\n",buffer);
		bzero(buffer,MESSAGE_SIZE*MESSAGE_SIZE);
		rcd(conn_dsc,argv[3]);
		rpwd(conn_dsc,buffer);
//		printf("%s\n",buffer);
		bzero(buffer,MESSAGE_SIZE*MESSAGE_SIZE);
		rls(conn_dsc,argv[3],buffer);
		printf("%s",buffer);
		free(buffer);
		disco(conn_dsc);
	}
	return 0;
}
