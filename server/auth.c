#include "auth.h"
#include "global.h"
#include "messages.h"
#include <string.h>
#include <stdlib.h>

 

void push_file(flist**, char*, char*,char*);
void add_users_from_line(flist**, char*); 

void push_file(flist **list, char* fn, char* usrs_r, char* usrs_w) {
    flist* fl_el;
    fl_el = (flist*) malloc(sizeof(flist));
    printf("Element, przydzielono pamięć:0x%X\n",fl_el);
    fl_el->users_read = 0;
    fl_el->users_write = 0;
    fl_el->next=NULL;
    fl_el->file_name = (char*) malloc((1+strlen(fn))*sizeof(char));
    strncpy(fl_el->file_name,fn,strlen(fn));
    if (usrs_r != NULL) {
        fl_el->users_read = (char*) malloc((1+strlen(usrs_r))*sizeof(char));
        strncpy(fl_el->users_read,usrs_r,strlen(usrs_r));
    }
    if (usrs_w != NULL) {
        fl_el->users_write = (char*) malloc((1+strlen(usrs_w))*sizeof(char));
        strncpy(fl_el->users_write,usrs_w,strlen(usrs_r));
    }
    if (*list == NULL) {
        *list = fl_el;
        return;
    }
    flist* tmp = *list;
    while ( tmp->next != NULL) 
        tmp = tmp->next;
    tmp->next = fl_el;
    logs("push:");
    logs(fn);
    logend();
}

int read_user_rights(flist **fl) {
static char line[2*MESSAGE_SIZE];
FILE *fp;
fp = fopen(RIGHTS_FILE,"r");
if (!fp) return -1;
while (fgets(line,2*MESSAGE_SIZE-1,fp) != NULL) {
    int len = strlen(line) - 1; //znak końca linii
    if (line[len] == '\n' || line[len]=='\r') 
        line[len] = '\0';
    add_users_from_line(fl,line);
}
fclose(fp);
}

int free_user_list(flist **fl) {
  flist *tmp;
  while (*fl != NULL)
  {
      tmp = *fl;
      *fl = tmp->next;
      if (tmp->file_name)
          free(tmp->file_name);
      if (tmp->users_read)
          free(tmp->users_read);
      if (tmp->users_write)
          free(tmp->users_write);
      printf("Zwalniam: 0x%X\n",tmp);
      free(tmp);
  }
}


void add_users_from_line(flist** fl, char* line) {
        inline char* skip_white(char* s) {
            while (*s == ' ' || *s == '\t') ++s;
            return s;
        }
    char *p = skip_white(line); 
    if (*p == '#') return; // komentarz
    char *fname,*usr_r,*usr_w;
    fname = strtok(p,":");
    printf("file: %s - ",fname);
    usr_r = strtok(NULL,":");
    printf("read: %s ",usr_r);
    usr_w = strtok(NULL,";");
    printf("write: %s;\n",usr_w);
    push_file(fl,fname,usr_r,usr_w);
}

int check_if_can_read(flist* fl,char *fn,char* user) {
while (fl) {
    if (strcmp(fl->file_name,fn)==0) 
        if (strstr(fl->users_read,user))
            return 1;
            else return 0;
    fl=fl->next;
    }
    return 1; //jak pliku nie ma na liście jest dostęp
}

int check_if_can_write(flist* fl,char *fn,char* user) {
while (fl) {
    if (strcmp(fl->file_name,fn)==0) 
        if (strstr(fl->users_read,user))
            return 1;
            else return 0;
    fl=fl->next;
    }
    return 1; //jak pliku nie ma na liście jest dostęp
}

int identified(char* name) {
    logs("Name for ident:");
    logsn(name);
	return 1;
    return (strcmp(name,"akasaka"))?0:1;
    }
