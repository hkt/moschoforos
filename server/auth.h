#ifndef __AUTH__H__
#define __AUTH__H__

typedef struct FILE_LIST {
    char* file_name;
    char* users_read;
    char* users_write;
    struct FILE_LIST* next;
} flist;



int identified(char*);
int read_user_rights(flist**);
int free_user_list(flist**);

#endif
