#include "dances_with_files.h"
#include "messages.h"
#include <fcntl.h>
#include <stdlib.h>


int open_file(char* fn,int flags) {
    int system_flags = 0;
    //system mode set to 666
    int system_mode = S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH;
    if (flags & O_WRITE) system_flags |= O_WRONLY;
    if (flags & O_READ) system_flags |= O_RDONLY;
        if (flags & O_WRITE && flags & O_READ) flags = O_RDWR;
    if (flags & O_ADD) {
        system_flags |= O_APPEND;
        return open(fn,system_flags);
    }
    if (flags & O_CREATE) {
        system_flags |= O_CREAT;
        return open(fn,system_flags,system_mode);
    }
    if (flags & O_TRUNCATE) {
        system_flags |= O_TRUNC;
        return open(fn,system_flags);
    }
	int ret_val = open(fn,system_flags);
	if ( flock(ret_val, LOCK_EX | LOCK_NB ) < 0 )
		return -2;
    return ret_val;
}


