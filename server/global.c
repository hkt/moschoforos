#include "global.h"
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>

inline void logs(char* s) {
   fprintf(stderr,"%s",s);
}

inline void logi(int i) {
    fprintf(stderr,"%d",i);
}

inline void logsn(char* s) {
    fprintf(stderr,"%s\n", s);
    fflush(stderr);
}

inline void logend() {
    fprintf(stderr,"\n");
    fflush(stderr);
}

inline void logh(char h) {
    fprintf(stderr,"0x%X",h);
    fflush(stderr);
}

int fd_lock;
static FILE* logfile;


int at_start() {
    if((fd_lock = open(LOCK_FILE, O_RDWR|O_CREAT|O_EXCL, 0666)) == -1)
		return -1;
	if(flock(fd_lock, LOCK_EX | LOCK_NB) == -1)
		return -1;
    // LOCK_EX - blokuje/sprawdza na wyłączność
    // LOCK_NB - (nonblocking) nie oczekuje na zwolnienie locka
    if((logfile = fopen("server.log", "a"))==NULL)
		return -1;
    logs("Utworzono lock"); logend();
	return 0;
}


void at_end() {
	fclose(logfile);
	flock(fd_lock,LOCK_UN);
    unlink(LOCK_FILE);
    exit(0);
}

