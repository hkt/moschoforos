#ifndef __GLOBAL_H__
#define __GLOBAL_H__
#include <stdio.h>


#define LOCK_FILE "/tmp/fserver.lock"
#define RIGHTS_FILE "auth.txt"
#define CAN_READ 2
#define CAN_WRITE 4

inline void logs(char*);
inline void logi(int);
inline void logsn(char*);
inline void logh(char);
inline void logend();

int  at_start();
void at_end();

#endif /* __GLOBAL__H__ */
