#include "loop.h"
#include <sys/socket.h>
#include <unistd.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <string.h>
#include "thread.h"
//#include <cnaiapi.h>

;
#define BUFFER_LEN 256

void main_loop() {
	int sock, newsock;
	socklen_t clilen;
	char buffer[BUFFER_LEN];
	struct sockaddr_in serv_addr, cli_addr;
	int n, yes=1;
	sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(yes));
	if (sock < 0) 
	{
		logs("ERROR opening socket");
		logend();
	}
	bzero((char *) &serv_addr, sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = INADDR_ANY;
	serv_addr.sin_port = htons(COMM_PORT);
	if (bind(sock, (struct sockaddr *) &serv_addr,
				sizeof(serv_addr)) < 0) 
	{
        // tu musimy sprawdzić PERMISSION denied
        // porty o nr  < 1024 
        // WELL KNOWN PORTS
        // może tylko root proces o nr roota
		logs("ERROR on binding");
		logend();
	}

	listen(sock,5);
	int pid, csock;
	struct sockaddr_in client_addr;
	socklen_t client_len = sizeof(client_addr);
	while(1){
		csock = accept(sock,(struct sockaddr *) &client_addr, 
				&client_len);
		if(csock <0){
			logsn("Error on accept");
			return;
		}
		pid = fork();
		if(pid<0){
			logsn("Error on fork");
		}
		if(pid == 0){
			close(sock); //zamknięcie deskryptora nasłuchowego
			procesor(csock);
			exit(0);
		}else close(csock);

	} /* End of while */
	close(sock);
	return; /* we never get here */
}
