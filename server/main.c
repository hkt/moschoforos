#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <sys/file.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <time.h>
#include "global.h"
#include "loop.h"


int main(int argc, char * argv[]) {
	if(at_start() == -1)
	{
		printf("Process already running!\n");
		return -1;
	}	
	signal(SIGTERM,at_end);
	signal(SIGINT,at_end);
	signal(SIGCHLD, SIG_IGN); // reap children to prevent zombies	
    // I was so tired that for the first time I read:
    // Rape children to pervert zombies.
    // Biedne zombie...
	main_loop();
	/*
	close(STDIN_FILENO);
	close(STDOUT_FILENO);
	close(STDERR_FILENO);
    */

	atexit(at_end);
	exit(EXIT_SUCCESS);
}
