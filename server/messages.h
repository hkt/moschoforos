#ifndef __MESSAGES__H_
#define __MESSAGES__H_

#define MESSAGE_SIZE 1024
#define SIZE_THRESHOLD 64 
#define H_SIZE 2
#define I_SIZE 4
#define MAX_TTL 60

#define REQ_HANDSHAKE 10 // Not used since ENIAC
#define RES_HANDSHAKE 11 // Not used since ENIAC
#define REQ_IDENTIFY  100
#define RES_IDENTIFY  101
/* drugi bajt w IDENTIFY */
#define OPERATION_OK 101 
#define OPERATION_NOPE 203

#define REQ_BYE 20
#define RES_BYE 21

#define REQ_PING 30
#define RES_PONG 31

/* OPERACJE PLIKOWE */

#define REQ_OPEN 132
#define RES_OPEN 133

#define REQ_READ 134
#define RES_READ 135

#define REQ_WRITE 136
#define RES_WRITE 137

#define REQ_CLOSE 138
#define RES_CLOSE 139

#define REQ_LSEEK 140
#define RES_LSEEK 141

/* FLAGI */

#define O_READ 0x01
#define O_WRITE 0x02
#define O_TRUNCATE 0x04
#define O_CREATE 0x08
#define O_ADD    0x10

/* OPERACJE KATALOGOWE */

#define REQ_CD 180
#define RES_CD 181

#define REQ_LS 182
#define RES_LS 183

#define REQ_PWD 184
#define RES_PWD 185

#endif
