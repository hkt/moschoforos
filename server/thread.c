#include "thread.h"
#include <stdlib.h>
#include "messages.h"
#include <stdio.h>
#include <string.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <sys/poll.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include "auth.h"
#include <stdint.h>
#include <errno.h>
#include "dances_with_files.h"
#include <fcntl.h>
#include <dirent.h>

char* enc_int(char *s,int32_t num) {
	union {
		int32_t i;
		char c[4];
	} bint;
	bint.i = htonl(num);
	memcpy(s,bint.c,4*sizeof(char));
	return s;
}

int32_t dec_int(char* s) {
	union {
		int32_t i;
		char c[4];
	} bint;
	memcpy(bint.c,s,4*sizeof(char));
	bint.i = ntohl(bint.i);
	return bint.i;
}


// Element listy poleceń dla serwera
typedef struct list_en {
	unsigned char message[MESSAGE_SIZE];
	struct list_en * next;
} list;

// Struktura dla jednego wątku
typedef struct connection_en {
	int con_fd;
	list* read_q;
	list* send_q;
	char status; //ACTIVE, CLOSED, DOUBTFUL,
	//CLOSING
	char* name;
	int ttl;
} connection;


void push(list **queue, unsigned char* message)
{
	list *msg =(list *) malloc(sizeof(list));
	bzero(msg->message, MESSAGE_SIZE*sizeof(unsigned char));
	/* if message value is lower than SIZE_THRESHOLD
	 * we copy only 2 bytes :-) */
	if (message[0] < SIZE_THRESHOLD) {
		msg->message[0] = message[0];
		msg->message[1] = message[1];
	}
	else {
		memcpy(msg->message,message,MESSAGE_SIZE*sizeof(unsigned char));
	}
	msg->next = NULL;
	if ((*queue) == NULL )
	{
		(*queue) = msg;
		return;
	}
	list *tmp = *queue;
	while ( tmp->next != NULL)
		tmp = tmp->next;
	tmp->next = msg;
	logs("[ PUSH ] pushed; wartość liście:");
	logi((*queue)->message[0]);
	logend();
};

void pop(list **queue)
{
	list *tmp = *queue;
	(*queue) = tmp->next;
	free(tmp);
};

void empty(list **queue)
{
	list *tmp = *queue;
	while(*queue != NULL )
	{
		tmp = *queue;
		(*queue) = tmp->next;
		free(tmp);
	}
};

void* receiver(void * arg)
{
	connection *client = (connection*) arg;
	int n;
	unsigned char buffer[MESSAGE_SIZE];
	struct pollfd ufds;
	ufds.fd = client->con_fd;
	ufds.events = POLLIN | POLLPRI;
	bzero(buffer,MESSAGE_SIZE);
	//Read message
	while( ( n=poll(&ufds, 1, 1000) ) >= 0  &&
			client->status != CLOSED &&
			client->status != CLOSING )
	{
		logsn("[ RECEIVER ] Reader loop");
		if ( n == 0 ) {
			(client->ttl)--;
			continue;
		}
		n=read(client->con_fd,buffer,MESSAGE_SIZE);
		if ( n<=0 && client->status != CLOSING )
		{
			client->status = CLOSED;
		}
		logsn("[ RECEIVER ] Pushing on read_q\n");
		push(&client->read_q,buffer);
		bzero(buffer,MESSAGE_SIZE);
	}
	//react to error on poll 
	if ( n < 0 && client->status != CLOSING )
	{
		client->status = CLOSED;
	}
};

void* sender(void * arg)
{
	logsn("[ SENDER ] thread");
	connection *client = (connection*) arg;
	int n;
	unsigned char buffer[MESSAGE_SIZE];
	//Read from queue
	switch (client->status) {
		case CLOSED: logsn("[ SENDER ] status closed"); break;
		case ACTIVE: logsn("[ SENDER ] status active"); break;
		case DOUBTFUL: logsn("[ SENDER ] status doubtful"); break;
		case CLOSING: logsn("[ SENDER ] status closing"); break;
	}
	while ((client->status == ACTIVE) || (client->status == DOUBTFUL))
	{   
		bzero(buffer,MESSAGE_SIZE*sizeof(unsigned char));
		logsn("[ SENDER ] bufor wyzerowany"); 
		while( client->send_q != NULL )
		{
			//Create message
			//strcpy(buffer,client->send_q->message); JAREK STYLE
			memcpy(buffer,client->send_q->message,
					MESSAGE_SIZE*sizeof(unsigned char));
			logs("Sending type: ");
			logi((int)buffer[0]);
			logend();
			//Pop queue
			pop(&client->send_q);
			//send
			//n = write(client->con_fd,buffer, strlen(buffer)); JAREK STYLE
			if (buffer[0] < SIZE_THRESHOLD)
				n = write(client->con_fd,buffer,H_SIZE*sizeof(unsigned char));
			else 
				n = write(client->con_fd,buffer,MESSAGE_SIZE*sizeof(unsigned char));
			if ( n <= 0 )
			{
				logs("[ SENDER ] Error sending");
				logi(n);
				logend();
				client->status = CLOSED;
				break;
			}
		}
		//sleep(1);
	}
	logsn("Koniec sendera");
};

void* procesor(int socket_fd)
{
	connection *client = (connection*) malloc(sizeof(connection));
	bzero(client,sizeof(connection));
	unsigned char buffer[MESSAGE_SIZE];
	client->con_fd = socket_fd;
	if (client->con_fd < 0) 
	{
		logsn("ERROR on accepting");
		return;
	}
	client->status = ACTIVE;
	bzero(buffer,MESSAGE_SIZE*sizeof(unsigned char));
	int n;
	n = read(client->con_fd,buffer,MESSAGE_SIZE*sizeof(unsigned char));
	if (n < 0) 
	{
		logsn("ERROR reading from socket");
		close(client->con_fd);
		return;
	}
	if ( buffer[0] != REQ_IDENTIFY )
	{
		logs("You're doing it wrong, first identify: |");
		logi(buffer[0]);
		close(client->con_fd);
		return;
	}
	buffer[0] = RES_IDENTIFY;
	if (strlen(buffer+H_SIZE)>1 && identified(buffer + H_SIZE)) {
		logsn(buffer+H_SIZE);
		client->name = (unsigned char*)
			malloc(strlen(buffer+H_SIZE)*sizeof(unsigned char));
		strcpy(client->name,buffer+H_SIZE);
		buffer[1] = OPERATION_OK;
	}
	else buffer[1] = OPERATION_NOPE;
	// log
	logs("Sending: "); logi(buffer[1]); logend();
	// log
	n = write(client->con_fd,buffer,MESSAGE_SIZE*sizeof(unsigned char));
	if (n < 0)
	{
		logsn("ERROR writing to socket");
		close(client->con_fd);
		return;
	}
	logi(n); logsn(" bytes sent");
	logs("Connected with client: ");
	logsn(buffer + H_SIZE);
	bzero(buffer,MESSAGE_SIZE*sizeof(unsigned char));

	//Create read and send queues
	client->read_q = NULL;
	client->send_q = NULL;
	client->ttl = MAX_TTL;

    //Read access file; create rights list
    flist *fl=0; 
    read_user_rights(&fl);

	//Start receiver && sender	
	pthread_t reader_t, sender_t;
	pthread_create(&reader_t, NULL, receiver,(void*) client);
	pthread_create(&sender_t, NULL, sender,(void*) client);

	//Process read queue
	while(client->status==ACTIVE || client->status==DOUBTFUL)
	{
		if (client->ttl < 0) {
			logsn("Client timed out");
			client->status = CLOSING;
			buffer[0] = REQ_BYE;
			buffer[1] = '\0';
			write(client->con_fd,buffer,H_SIZE*sizeof(unsigned char));  
			close(client->con_fd);
			break;
		}
		bzero(buffer,MESSAGE_SIZE);
		unsigned char msg_type;
		unsigned char* msg;
		int flags, ret_val, req_count;
		if ( client->read_q != NULL )
		{
			msg_type = client->read_q->message[0];
			msg = client->read_q->message; 
			switch(msg_type)
			{
				case REQ_PING:
					client->ttl = MAX_TTL;
					buffer[0] = RES_PONG;
					buffer[1] = '\0';
					push(&client->send_q,buffer);
					client->status = ACTIVE;
					logsn("Received ping");
					break;
				case REQ_BYE:
					logsn("Received REQ_BYE");
					client->status = CLOSING;
					buffer[0] = RES_BYE;
					buffer[1] = '\0';
					write(client->con_fd,buffer,H_SIZE*sizeof(unsigned char));  
					close(client->con_fd);
					continue;
				case REQ_OPEN:
					logsn("Received REQ_OPEN");
					client->ttl = MAX_TTL;
					buffer[0] = RES_OPEN;
					buffer[1] = OPERATION_OK;
                    int flags = dec_int(msg+H_SIZE);
                    if (flags & O_READ)  
                        if(!check_if_can_read(fl,msg+H_SIZE+I_SIZE,client->name)){
                            buffer[1] = OPERATION_NOPE;
                            push(&client->send_q,buffer);
                            logsn("Blad autoryzacji (read)");
                        }
                    if (flags & O_WRITE) 
                        if(!check_if_can_write(fl,msg+H_SIZE+I_SIZE,client->name)){
                            buffer[1] = OPERATION_NOPE;
                            push(&client->send_q,buffer);
                            logsn("Blad autoryzacji (write)");
                        }
                    //openfile(filename,flags)
					ret_val = open_file(msg+H_SIZE+I_SIZE,
							dec_int(msg+H_SIZE));
					enc_int(buffer+H_SIZE,ret_val);
					logs("Deskryptor: "); logi(ret_val); logend();
					push(&client->send_q,buffer);
					break;
				case REQ_READ:
					logsn("Received REQ_READ");
					client->ttl = MAX_TTL;
					buffer[0] = RES_READ;
					buffer[1] = '\0';
					req_count = dec_int(msg+H_SIZE+I_SIZE);
					if (req_count > MESSAGE_SIZE-H_SIZE-I_SIZE)
						req_count = MESSAGE_SIZE-H_SIZE-I_SIZE;
					/* Jeżeli poproszą nas o więcej, damy tyle ile możemy
					 * WAIT for next request! */
					ret_val = read(dec_int(msg+H_SIZE),buffer+H_SIZE+I_SIZE,
							req_count*sizeof(unsigned char));
					logi(ret_val); logsn(" bytes read");
					if (ret_val < 0) {
						logs("Errno: "); logi(errno); logend();
					}
					enc_int(buffer+H_SIZE,ret_val);
					push(&client->send_q,buffer);
					break;
				case REQ_WRITE:
					logsn("Received REQ_WRITE");
					client->ttl = MAX_TTL;
					buffer[0] = RES_WRITE;
					buffer[1] = '\0';
					ret_val = write(dec_int(msg+H_SIZE),msg+H_SIZE+I_SIZE+I_SIZE,
							dec_int(msg+H_SIZE+I_SIZE)*sizeof(unsigned char));
					logs("Written buffer: ");logsn(msg+H_SIZE+I_SIZE+I_SIZE);
					logs("Bytes count: ");logi(ret_val);logend();
					enc_int(buffer+H_SIZE,ret_val);
					push(&client->send_q,buffer);
					break;
				case REQ_CLOSE:
					logsn("Received REQ_CLOSE");
					client->ttl = MAX_TTL;
					buffer[0] = RES_CLOSE;
					buffer[1] = '\0';
					ret_val = close(dec_int(msg+H_SIZE));
					flock(dec_int(msg+H_SIZE),LOCK_UN);
					enc_int(buffer+H_SIZE,ret_val);
					push(&client->send_q,buffer);
					break;
				case REQ_LSEEK:
					logsn("Received REQ_LSEEK");
					client->ttl = MAX_TTL;
					buffer[0] = RES_LSEEK;
					buffer[1] = '\0';
					ret_val = lseek(dec_int(msg+H_SIZE),dec_int(msg+H_SIZE+I_SIZE),
							dec_int(msg+H_SIZE+I_SIZE+I_SIZE));
					logs("Lseeked with result: ");logi(ret_val);
					logend();
					enc_int(buffer+H_SIZE,ret_val);
					push(&client->send_q,buffer);
					break;
				case REQ_CD:
					logsn("Received REQ_CD");
					client->ttl = MAX_TTL;
					buffer[0] = RES_CD;
					ret_val = chdir(msg+H_SIZE);
					if(ret_val == 0){
						buffer[1]=OPERATION_OK;
					}else{
						buffer[1]=OPERATION_NOPE;
					}
					logs("changed dir with result: ");logi(ret_val);
					logend();
					push(&client->send_q,buffer);
					break;
				case REQ_LS:
					logsn("Received REQ_LS");
					client->ttl = MAX_TTL;
					bzero(buffer,MESSAGE_SIZE);
					DIR *pdir = NULL;
					pdir = opendir (msg+H_SIZE);
					struct dirent *pent = NULL;
					pent = readdir(pdir);
					while (pent) 
					{
						client->ttl = MAX_TTL;
						//bzero(buffer,MESSAGE_SIZE);
						buffer[0]=RES_LS;
						buffer[1]=OPERATION_OK;
						strcat(buffer+H_SIZE, pent->d_name);
						strcat(buffer+H_SIZE, "\n");
						push(&client->send_q,buffer);
						logs(buffer+H_SIZE);
						bzero(buffer,MESSAGE_SIZE);
						pent = readdir(pdir);
					}
					closedir (pdir);
					buffer[0]=RES_LS;
					buffer[1]=OPERATION_OK;
					buffer[2]='\0';
					push(&client->send_q,buffer);
					break;
				case REQ_PWD:
					logsn("Received REQ_PWD");
					client->ttl = MAX_TTL;
					buffer[0] = RES_PWD;
					if(getcwd(buffer+H_SIZE,MESSAGE_SIZE-H_SIZE)){
						buffer[1]=OPERATION_OK;
						logsn("Get current directory successful");
					}else{
						buffer[1]=OPERATION_NOPE;
						logsn("Get current directory failed (null pointer)");
					}
					push(&client->send_q,buffer);
					break;
				default:
					logs("Unknown message: ");
					logi((int)msg_type);
					logend();
					break;
			}
			pop(&client->read_q);
		}
		while ( client->read_q == NULL && client->status != CLOSED)
			if ( client->send_q != NULL ) //Czekamy aż sender wyśle
			{
				//sleep(1);
				continue;
			}
	}
	//JOIN THREADS 
	logsn("Joining threads");
	pthread_join(reader_t,NULL);
	pthread_join(sender_t,NULL);
	logsn("Emptying queues");
	empty(&client->read_q);
	empty(&client->send_q);
	free(client->name);
	free(client);
    free_user_list(&fl);
	logsn("Closing connection");	
	close(client->con_fd);
}
