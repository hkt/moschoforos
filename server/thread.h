#ifndef __THREAD__H_
#define __THREAD__H_
#include <pthread.h>
#include <stdint.h>

#define CLOSED 0
#define ACTIVE 1
#define DOUBTFUL 2
#define CLOSING 3

void* receiver();
void* sender();
void* procesor();

char* enc_int(char*,int32_t);
int32_t dec_int(char*);

#endif
